# README FOR THE JAMBO DATA CAPTURING WEB APPLICATION#

* This is a data capturing application that I have developed for a red blade software interview.
* Version 0.0.1

- Set up requirements: NodeJS, AngularJS, MongoDB and Express;
- Not for production: Code is all over the place. 

#ASSUMPTIONS#
* -users, essentially anyone who is using the app will have to log in. so i included log in and register functionality.
* -I havent added any password hashing in the application due to time constraints.
* -The app is using the http protocal and thus your browser won't like the login page. but the production level application should obviously be implemented with https. 
* -user password has not been hashed at the time of writing
* -Token based authentication is employed. i.e. server sends a token, the token is stored in the local storage of the client's browser. Token is appended in the header on any request made through the app. The token has a 24 hour expiration time.
* -The user would also need to update, edit and delete records and thus this functionality has been added.


#DATABASE SETUP#

- Configure your local database file for the app by creating a database with the two collections as below. 
- Database Name: JAMBO
- Collection 1: USERS;  //the information belonging to the people who register to use the application.
- Collection 2: CAPTUREDRECORDS; //where all the data a user captures is stored

#IMPORTANT#
* TO USE THE APP; YOU MUST FIRST REGISTER ELSE THE SERVER WILL NOT LIKE YOU. 
* CLICK REGISTER, REMEMBER YOUR PASSWORD. Otherwise you'll have to take a look in the database yourself..(yeah sorry passwords haven't yet been hashed at the time of writing)

#KNOWN BUGS#

* *TRY NOT TO PRESS F5(UNLESS YOU'RE HAPPY WITH LOGING IN AGAIN).. THE AUTHENTICATION TOKEN WILL BE LOST AND MR SERVER DOESN'T LIKE IT-- WELL IT ACTUALLY IS IN THE LOCAL STORAGE BUT i HAVEN'T yet PASSED IT VIA THE URL. (WAS TRYING TO MEET A DEADLINE). 
* * YOU MIGHT HAVE TO CLICK TWICE BETWEEN THE CAPTURE MODE AND THE VIEW AND EDIT MULTIPLE RECORDS MODE. OH BTW I DIDN'T FORGET THE LOG OUT BUTTON EITHER - LOL.
* *quite a number more i'm sure.
* 
#OVERALL:
* *THERE ARE A NUMBER OF AREAS WHERE CODE IS REPETITIVE. COULD WRITE SOME SERVICES TO EASE THE LOAD. LOTS OF UNNECESSARY CONSOLE.LOGS TOO.. PLEASE FORGIVE ME! WILL UPDATE IN THE NEXT VERSION. ALSO PLANNING TO DEPLOY ngMessage in the next version.

#RANDOM
* *Jambo is hi in swahili :). So JAMBO!!
* 
* *Any queries can be forwarded to alfy1502@gmail.com.
* 
* *I'm sure you'll have fun reading through my illogical code!
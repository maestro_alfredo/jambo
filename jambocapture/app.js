require('rootpath')();
var express = require('express');
var app = express();
var session = require('express-session');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var config = require('config.json');
var mongoConnect = require('mongoose'); // to install the mongoose.
var fs = require('fs');

app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({ secret: config.secret, resave: false, saveUninitialized: true }));
//viewengine
app.set('views',__dirname+'/public/views')
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// use JWT auth to secure the api
//app.use('/', expressJwt({ secret: config.secret }).unless({ path: ['/api/users/authenticate', '/register'] })); //ignore the register part for authentication

//maps the links to the serverside controlllers controllers
app.use('/register', require('./controllers/register'));
app.use('/login', require('./controllers/login'));
app.use('/jambo', require('./controllers/jambo'));
//app.use('/register', require('./services/userservice'));
//app.use('/login', require('./controllers/login.controller'));
//app.use('/register', require('./controllers/register.controller'));
//app.use('/app', require('./controllers/app.controller'));
//app.use('/api/users', require('./controllers/api/users.controller'));

app.listen(3000);
console.log("server running on port 3000");
console.log("register trying");

var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('config.json');
var mongojs = require('mongojs');
var db = mongojs('JAMBO', ['USERS']);
var server = require('mongodb').Server;


router.get('/', function (req, res) {
    res.render('index.html');
    console.log("fetched data successfully");
});

router.post('/', function (req, res) {
    // register using api to maintain clean separation between layers
    console.log("Trying to post meaningful data");
    console.log("First Name is "+req.body.firstName);
    console.log("Last Name is "+req.body.lastName);
    console.log("email address is "+req.body.email);
    console.log("the database connectionString is "+config.connectionString);

    //check if the collection contains the same user.
    var checkExists = db.USERS.findOne({ email: req.body.email}, function(err, document){

        if(err){
            console.log("Mongodb Error");
            res.json({message: "Registration unsuccessful. Check with administrator", regerr: true});

            return false;
        }

        //if doesn't exist a new user has been detected
        if(!document){
            console.log("New user detected; Updating database");
            db.USERS.insert(req.body, function(err, doc){
            res.json({message: "succesful registration. Proceed to log in with your details", regsuccess: true});
    

            });
            return false;
        }
        else{
                    //user already exists
            //console.log("Registration unsuccessful. Username exists.");
            res.json({message: "Registration unsuccessful. Email in use", regerr: true});
            return true;


        }



    });

    //send a message that the registration is successful
    //res.json({message: "succesful registration. Proceed to log in with your details"});
});

module.exports = router; 
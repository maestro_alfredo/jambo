//put these just to double check that the scripts were being loaded correctly;
console.log("jambo kicking in ");

var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('config.json');
var mongojs = require('mongojs');
var db = mongojs('JAMBO', ['CAPTUREDRECORDS']);
var server = require('mongodb').Server;
var jwt= require('jsonwebtoken'); //token for authentication
var decode = false;


router.put('/:id', function(req, res) {
    var token = req.body.token || req.query.token || req.headers['x-access-token']; //duplication for all authentication requests(get, put, delete and post) of course... So many ways I can improve this but I was going for a minimalist approach
    if(token){
        jwt.verify(token, config.secret, function(err, decoded){
            if(err){
                return res.json({ success: false, message: 'Failed to authenticate token.' }); 
            }
            else{
                //check if the database contains a user with the same id if so then do something. 
                db.CAPTUREDRECORDS.findOne({id: req.body.id}, function(err, doc){
                    if(err){
                        return res.json({success: false, error: err});
                    }
                    //if there isn't any result then its not an update. its a new user. So tell client to insert a new record instead
                    if(!doc){
                        return res.json({success: false, message: "User no longer exists"});
                    }
                    db.CAPTUREDRECORDS.update({id: req.body.id}, {firstName: req.body.firstName, surname: req.body.surname, id: req.body.id, contact: { phone: req.body.contact.phone, email : req.body.contact.email, address: req.body.contact.address }} , function(err, doc){
                        if(err){
                            return res.json({success: false, error: err});
                        }
                        else{
                            return res.json({success: true, message: "Successfully updated the record"});
                        }

                    });
                });   

            }

        });

    }
    else{

        return res.status(403).send({ 
            success: false, 
            message: 'No token provided.' 
        });

    }
    // body...
});



router.get('/', function (req, res) {

    console.log(req.headers);
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    //console.log(req);

      // decode token
      if (token ) {

        // verifies secret and checks exp
        jwt.verify(token, config.secret, function(err, decoded) {      
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });    
        } else {
            console.log("request authenticated");// if everything is good, save to request for use in other routes
            console.log("decoded"+ decoded);
            req.decoded = decoded;   
            db.CAPTUREDRECORDS.find(function(err, cursor){
                if(err){

                  return res.json({message: "cannot request for data", error: true})
              }
              return res.json({message: "data requested successfully", token: token, capturedrecords: cursor});
          });
        }
    });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({ 
            success: false, 
            message: 'No token provided.' 
        });
        
    }
    //r//es.render('index.html');
    //c//onsole.log("fetched data successfully");
});

router.post('/', function (req, res) {
    // register using api to maintain clean separation between layers
    console.log("Received user data");
    console.log("first Name of user "+req.body.firstName);
    console.log("surname of user "+req.body.surname);
    console.log("id of user "+req.body.id);
    console.log("Address of user "+req.body.address);
    console.log("token: "+req.headers['x-access-token']);
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    //console.log(req);

      // decode token
      if (token ) {

        // verifies secret and check errors
        jwt.verify(token, config.secret, function(err, decoded) {      
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });    
        } else {
            console.log("request authenticated");// if everything is good, save to request for use in other routes
            console.log("decoded"+ decoded);
            req.decoded = decoded;   

               //check if the collection contains the same user.
               var checkExists = db.CAPTUREDRECORDS.findOne({ id: req.body.id}, function(err, document){
                if(err){
                    console.log("Mongodb Error");                        
                    return res.json({message: "Cannot upload to database . Check with administrator", fail : true})};

                    //if doesn't exist a new user has been detected
                    if(!document){

                        db.CAPTUREDRECORDS.insert(req.body, function(err, doc){
                            res.json({message: "Data uploaded successfully", success: true});    

                        });
                    }
                    else{
                      return res.json({
                          fail: true,
                          message: "User with that id already exists",                                                  
                      });

                  }
              });
           }
       });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({ 
            success: false, 
            message: 'No token provided.' 
        });
        
    }
});

router.delete("/:id",function(req, res){

        console.log(req);
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

          // decode token
          if (token ) {

        // verifies secret and checks exp
        jwt.verify(token, config.secret, function(err, decoded) {      
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });    
        } else {
            console.log("request authenticated");// if everything is good, save to request for use in other routes
            console.log("decoded"+ decoded);
            req.decoded = decoded;   
            db.CAPTUREDRECORDS.findOne({id: req.params.id}, function(err, doc){
                if(err){

                  return res.json({message: "cannot request for data", error: true})
              }
              if(!doc){
                return res.json({message: "record might have been deleted by another user"});
            }
            db.CAPTUREDRECORDS.remove({id: req.params.id}, {justOne: true}, function(err, data){

                return res.json({message: "data removed successfully"});
            })
        });
        }
    });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({ 
            success: false, 
            message: 'No token provided.' 
        });
        
    }
});

module.exports = router; 
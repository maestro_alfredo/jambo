console.log("log in trying");

var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('config.json');
var mongojs = require('mongojs');
var db = mongojs('JAMBO', ['USERS']);
var server = require('mongodb').Server;
var jwt    = require('jsonwebtoken'); //token for authentication


router.get("/", function (req, res) {
    return res.redirect('/');    
});

router.post("/", function (req, res) {
    console.log("Trying to log into app");


    //check if the collection contains the same user. //NOT TO BE DONE IN PRODUCTION AS THIS IS VERY INSECURE... 
    var checkExists = db.USERS.findOne({ email: req.body.email, password: req.body.password}, function(err, document){

        if(err){
            console.log("Mongodb Error");
            
            return res.json({message: "Log-in unsuccessful. Check with administrator", logerr: true})};


        //if doesn't exist a new user has been detected
        if(!document){
            console.log("Invalid name or user combination");
            return res.json({message: "invalid name and user combination", logerr: true});
        }
        else{
                    //user already exists
                    console.log("Log in successful");
                    var token = jwt.sign(document, config.secret, {
                    expiresIn: 60*60*24 // expires in 24 hours
                    });

                    // return the information including token as JSON

                    db.CAPTUREDRECORDS.find(function(err, cursor){
                    if(err){

                      return res.json({message: "cannot request for data", error: true})
                    }
                    return res.json({message: "data requested successfully", success: true, token: token, capturedrecords: cursor, email: req.body.email});
                    });                       
                }
            });
});

module.exports = router; 
//can add 'ng-messages for validation messages here'
angular.module('app.login',['ngRoute', 'ngMaterial', 'ngAnimate', 'uiGmapgoogle-maps', 'nemLogging', 'ngAria','ngMessages']).config(function($routeProvider, $httpProvider, $windowProvider, $locationProvider, uiGmapGoogleMapApiProvider, $mdThemingProvider) {
	$routeProvider
	.when("/", {
		templateUrl : "/views/login.html",
		controller : "loginController"
	})
	.when("/register", {
		templateUrl : "/views/register.html",
		controller : "registerController"
	})
	.when("/jambo", {
		templateUrl : "/views/jambo.html",
		controller : "jamboController"
	});

	//added in order to prevent the # in the link location
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});

	uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyBqtRuMOV7OuM6hO4i-_RZEDnUzjm555fk',
        v: '3.20', //defaults to latest 3.X anyhow
        //libraries: 'weather,geometry,visualization'
    });


    $mdThemingProvider.theme('default').primaryPalette('pink').accentPalette('orange');

	//intercepts headers and adds an autorization token
	 $httpProvider.defaults.headers.common.Authorization = $windowProvider.$get().localStorage['x-access-token'];

});

//service that helps share properties between controllers... Was playing around to see how you could pass variables from one controller to another. 
//Probably Unecessary by the time you're reading this.
angular.module('app.login').factory('userslist', function(){

	var userslist = {};
	var property = 'First';
	 return {
            getProperty: function () {
                return property;
            },
            setProperty: function(value) {
                property = value;
            }
        };
});

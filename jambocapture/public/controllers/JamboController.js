//creating the controller for the login page
angular.module('app.login').controller('jamboController', ['$scope', 'userslist', 'uiGmapGoogleMapApi', '$location', '$window', '$timeout', '$http', '$mdDialog', function($scope, userslist, uiGmapGoogleMapApi, $location, $window, $timeout, $http, $mdDialog){
//for my own testing purposes.... Added to make sure that
console.log("jambo controller up and running");
$scope.viewMode = false; //flag to indicate the user is in view mode
$scope.captureMode = true; //boolean to say whether the user is in capture mode or not. Not being used at the moment
$scope.map = { center: { latitude: -33.918861, longitude: 18.423300 }, zoom: 8 };
$scope.ResultMode = false;
$scope.ViewText =  "View";



//array with the sections of the navigation we want

var geocoder = new google.maps.Geocoder;
$scope.marker = {
  id: 0,
  coords: {
    latitude: -33.918861,
    longitude: 18.423300
},
options: { draggable: true },
events: {
    dragend: function (marker, eventName, args) {

        console.log("User has finished dragging")
        console.log("Latitude is : "+marker.getPosition().lat());
        console.log("Longitude is: "+marker.getPosition().lng());
        var vlat = marker.getPosition().lat();
        var vlng = marker.getPosition().lng();

        getAddressFromCordinates(vlat,vlng);
      }
  }
};

//this gets a coordinate from an address
function getAddressFromCordinates(latitude, longitude){

    geocoder.geocode({'location': {lat: latitude, lng: longitude}}, function(results, status){
        if(status == "OK"){
            if(results[1]){
                console.log("The address found is "+results[1].formatted_address);
                console.log(results[1]);
                if($scope.user!=null){
                   $scope.user.contact.address = results[1].formatted_address;
               }
               refresh();
           }
           else{
            console.log("NOPE, nothing there");
        }
    }
    else{
        console.log("Geocoder failed due to "+status);

    }
});

}

//this gets an address from a coordinate
$scope.findAddress = function getCoordinatesFromAddress(address){

    geocoder.geocode({'address': address}, function(results, status){
        if(status == "OK"){
            var lat = (results[0].geometry.location.lat());
            var lon =   (results[0].geometry.location.lng());
            var marker = {
                id: Date.now(),
                coords: {
                    latitude: lat,
                    longitude: lon
                }
            };     
            $scope.map.center = {latitude: lat, longitude: lon };
            $scope.marker.coords = {latitude: lat, longitude: lon };
            refresh();
           //$scope.map.markers.push(marker);           
       }
       else{
        console.log("Geocoder failed due to "+status);
    }
});
}

//$scope.CAPTUREDUSERS = userslist.getProperty();
refresh(); //refresh on every reload;

//function to allow editing items in the table
$scope.editItem = function (person) {
    person.editing = true;
}

$scope.doneEditing = function (person) {
    person.editing = false;
    
};







console.log($scope.marker);

$scope.updateUser = function (person){

    $http({
        method: 'PUT',
        url: "/jambo/"+person.id,
        headers: {
            'x-access-token':  $window.localStorage.getItem('x-access-token')
        },
        data: person
    }).then(function(response){
        refresh();
    });
}

$scope.removeUser = function (inputID){

    $http({
        method: 'DELETE',
        url: "/jambo/"+inputID,
        headers: {
            'x-access-token':  $window.localStorage.getItem('x-access-token')
        }
    }).then(function(response){
        console.log(response.data.message);
        refresh();
    });
}

$scope.showSearch = function(){
    $scope.ResultMode = !$scope.ResultMode;
    if($scope.ResultMode){
        $scope.ViewText = "Capture";
    }
    else{
        $scope.ViewText = "View";
    }
}

function refresh(){
    $http({
        method: 'GET',
        url: '/jambo',
        headers: {
            'x-access-token': $window.localStorage.getItem('x-access-token')
        }

    }).then(function(response){
        //extends captured users to have an is editing property
        $scope.CAPTUREDUSERS = response.data.capturedrecords.map(function(obj) {
            return angular.extend(obj, {editing: false});
        });       
        console.log(response);
    });
}

$scope.submitform = function(){
    console.log("Posting......");
//getting the token from the local storage and passing it through  to the header of the request for authentication
$scope.token = $window.localStorage.getItem('x-access-token');
$http({
    method: 'POST', 
    url: '/jambo',    
    headers: {
        'x-access-token': $scope.token
    },
    data: $scope.user
}).then(function(response) {
    //obtaining the message from the server to be displayed on the alert <div>
    $scope.message = response.data.message;
    //obtaining the success or failure of the query in order to show the alert <div> in the jambo html
    //can be written much better of course
    $scope.success = response.data.success;
    $scope.fail = response.data.fail;
    $scope.user.firstName = "";
    $scope.user.surname = "";
    $scope.user.id = "";
    $scope.user.contact.email = "";
    $scope.user.contact.phone = "";
    $scope.user.contact.address = "";
    $scope.captureForm.$setPristine();
    $scope.captureForm.$setUntouched();
    
})
};
}]);
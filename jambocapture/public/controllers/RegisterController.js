//creating the controller for the register page
angular.module('app.login').controller('registerController', ['$scope', '$http', function($scope, $http){

	console.log("register controller up and running");
  $scope.message = "";
  $scope.regsuccess = "";
  $scope.regerr = "";

//define a function for registration

//handling when the user posts 
$scope.submitform = function(){

	$http({
		method  : 'POST',
		url     : '/register',
          data    : $scope.user, //forms user object          
      })
	.then(function(data) {
		if (data.errors) {
              // Showing errors.
              $scope.errorName = data.errors.name;
              $scope.errorUserName = data.errors.username;
              $scope.errorEmail = data.errors.email;              
          } else {
            console.log("form posted successfully");
            console.log(data.data.message);
          	$scope.message = data.data.message;
            $scope.regerr = data.data.regerr;
            $scope.regsuccess = data.data.regsuccess;
            $scope.user="";
          }
      });
};

}]);
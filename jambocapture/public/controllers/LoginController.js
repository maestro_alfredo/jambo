//creating the controller for the login page
angular.module('app.login').controller('loginController', ['$scope', 'userslist', '$location', '$window', '$http', function($scope, userslist, $location, $window, $http){

console.log("login controller up and running");

$scope.submitform = function(){

	$http({
		method  : 'POST',
		url     : '/login',
          data    : $scope.user, //forms user object          
      })
	.then(function(data) {
		if (data.errors) {
              // Showing errors.
              $scope.errorName = data.errors.name;
              $scope.errorUserName = data.errors.username;
              $scope.errorEmail = data.errors.email;              
          } else {
            console.log("login data posted successfully");
            console.log(data.data.message);
          	$scope.message = data.data.message;
            $scope.logerr = data.data.logerr;
            $scope.logsuccess = data.data.logsuccess;
            $scope.token = data.data.token;            
            $http.header = {"x-access-token" : data.data.token};
            $window.localStorage['x-access-token'] = data.data.token; //saving to local storage

            //if the response is successful, redirect to the jambo page. 
            if(data.data.success){
              userslist.setProperty(data.data.capturedrecords);
              $location.path('/jambo')
            }
           //$http.url('/jambo');
          }
      })

};

}]);